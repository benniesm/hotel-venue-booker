const faker = require('faker');
const uuid4 = require('uuid4');
const axios = require('axios');
const fs = require('fs');

const category = ['low', 'medium', 'high'];
const op = 	['free parking','free wifi','pets','restaurant','gym','pool','spa'];
const imageFiles = fs.readdirSync('../images/');

for (let c = 0; c < 6; c ++) {
  let id = uuid4();
  let name = faker.company.companyName() + ' Hotels';
  let description = faker.lorem.sentence();
  let distance_to_venue = faker.random.number({'min':500, 'max': 1000});
  let rating = faker.random.number({'min': 0, 'max': 5, 'precision': 0.1});
  let price_category = category[Math.floor(Math.random() * 3)];

  let no_of_amenities = Math.floor((Math.random() * 7) + 1);
  let amenities = [];
  for (let d = 0; d < no_of_amenities; d ++) {
    amenities.push(op[d]);
  }

  let images = [];
  images.push(imageFiles[c]);
  for (let f = 0; f < 2; f ++) {
    images.push(imageFiles[Math.floor(Math.random() * 6)]);
  }

  let rooms = [];
  let no_of_rooms = Math.floor((Math.random() * 5) + 2);
  for (let e = 0; e < no_of_rooms; e ++) {
    let room_id = uuid4();
    let room_name = faker.lorem.word();
    let room_description = faker.lorem.sentence();
    let max_occupancy = Math.floor((Math.random() * 4) + 1);
    let price_in_usd = faker.random.number({
      'min': 50, 'max': 100, 'precision': 0.01
    });

    rooms.push({
      id: room_id,
      name: room_name,
      description: room_description,
      max_occupancy: max_occupancy,
      price_in_usd: price_in_usd
    });
  }

  const hotel = {
    id: id,
    name: name,
    description: description,
    distance_to_venue: distance_to_venue,
    rating: rating,
    price_category: price_category,
    amenities: amenities,
    images: images,
    rooms: rooms
  };

  axios.post('http://localhost:4000/hotels', hotel)
  .then(r => console.log(r))
  .catch(e => console.log(e));

}
