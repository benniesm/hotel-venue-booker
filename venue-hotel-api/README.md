This is json-server project as a mock API generator.

## Installation (local machine)
Run `npm install`
This will create the `node_modules` directory and install all required dependencies.

#Running the application (local)
Run `json-server -p 4000 db.json`
This will start up the application on port 4000.
The default port is 3000.
