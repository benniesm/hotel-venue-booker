This project is a simple Hotel Booking application, built with React.js
Operations should be performed in the `hotel-booker` directory

## Installation (local machine)
Run `yarn install`
This will create the `node_modules` directory and install all required dependencies.

#Running the application (local and network)
Run `yarn start`
This will start up the application on port 3000.
