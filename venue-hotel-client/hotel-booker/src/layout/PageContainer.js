import React, { Component } from 'react';
import Body from './Body';
import Footer from './Footer';

class PageContainer extends Component {
  render() {
    return (
      <div className="Container">
        <Body />
        <Footer />
      </div>
    )
  }
}

export default PageContainer;
