import React from 'react';
import en from '../languageData/English';

const Footer = () => {
  return (
    <div className="Footer">
      {en.footer.text + ' - Leipzig 2020.'}
    </div>
  )
}

export default Footer;
