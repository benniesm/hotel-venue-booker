import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from './Header';
import Index from '../pages/Index';
import ViewHotel from '../pages/ViewHotel';
import ConfirmHotel from '../pages/ConfirmHotel';
import AdminIndex from '../admin/AdminIndex';
import CreateHotel from '../admin/CreateHotel';

const Body = () => {
  return (
  <Router>
    <Header />
    <div className="Body">
    <Switch>
      <Route path="/" exact component={Index} />
      <Route path="/view-hotel" component={ViewHotel} />
      <Route path="/confirm-hotel" component={ConfirmHotel} />
      <Route path="/admin-index" component={AdminIndex} />
      <Route path="/create-hotel" component={CreateHotel} />
    </Switch>
    </div>
  </Router>
  )
}

export default Body;
