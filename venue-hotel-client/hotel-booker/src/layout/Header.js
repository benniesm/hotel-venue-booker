import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import {
  mapStateToProps,
  mapDispatchToProps
} from '../store/StateDispatch';
import '../App.css';
import en from '../languageData/English';

class HeaderComp extends Component {
  render() {
    return (
      <div className="Header">
        <div className="Menu">
          <Link to="/" className="Link">{en.menu.home}</Link>
        </div>
        <div className="User">
          <span>
          {en.user.message + ', '}
          {this.props.state.page.auth.first_name + '.'}
          </span>
        </div>
      </div>
    )
  }
}

const Header = connect(mapStateToProps, mapDispatchToProps)(HeaderComp);
export default Header;
