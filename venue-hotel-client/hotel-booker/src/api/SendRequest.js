import axios from 'axios';
import { serverUrl } from './ServerParams';

const sendRequest = async(method, endPoint, params) => {
  const options = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  const address = serverUrl + endPoint;

  const serverErr = {
    statusText: 'Remote',
    data: { error: 'Process Error' }
  }

  const respondBack = (apiResult) => {
    if (apiResult.status >= 299) {
      return serverErr;
    }

    return apiResult;
  }

  const createRequest = async() => {
    const apiResult = await axios.post(address, params, options).then(resp => {
      return resp;
    }).catch(err => {
      return err;
    });

    return respondBack(apiResult);
  }

  const readRequest = () => {
    const apiResult = axios.get(address, params, options).then(resp => {
      return resp;
    }).catch(err => {
      return err;
    });

    return respondBack(apiResult);
  }

  const deleteRequest = () => {
    const apiResult = axios.delete(address, params, options).then(resp => {
      return resp;
    }).catch(err => {
      return err;
    });

    return respondBack(apiResult);
  }

  switch (method) {
    case 'post':
      return createRequest();
    case 'get':
      return readRequest();
    case 'delete':
      return deleteRequest();
    default:
      console.log('no request');
  }
}

export default sendRequest;
