import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  mapStateToProps,
  mapDispatchToProps
} from '../store/StateDispatch';
import sendRequest from '../api/SendRequest';
import en from '../languageData/English';

class IndexRowComp extends Component {
  constructor() {
    super();
    this.state = {
      distance: '',
      rating: '',
      price_category: '',
      amenities: '',
      more: 'none',
      width: '65%'
    }
  }

  componentDidMount() {
    this.getHotels();
    this.getUser();
  }

  filterDistance = async(evt) => {
    await this.setState({
      distance: parseInt(evt.target.value)
    });
    let fHotels = this.props.state.hotel.allHotels ?
      this.props.state.hotel.allHotels.filter(fh => {
        return fh.distance_to_venue <= this.state.distance
      })
      :
      [];
    this.props.setHotelsFilter(fHotels);
  }

  filterRating = async(evt) => {
    await this.setState({
      rating: parseInt(evt.target.value)
    });
    let fHotels = this.props.state.hotel.allHotels ?
      this.props.state.hotel.allHotels.filter(fh => {
        return fh.rating <= parseInt(this.state.rating)
      })
      :
      [];
    this.props.setHotelsFilter(fHotels);
  }

  filterPrice = async(evt) => {
    await this.setState({
      price_category: evt.target.value
    });
    const aHotels = this.props.state.hotel.allHotels ?
      this.props.state.hotel.allHotels : [];
    if (this.state.price_category === "all") {
      this.props.setHotelsFilter(aHotels);
      return;
    }

    let fHotels = aHotels.filter(fh => {
      return fh.price_category === this.state.price_category
    });
    this.props.setHotelsFilter(fHotels);
  }

  filterAmenities = async(evt) => {
    await this.setState({
      amenities: evt.target.value
    });
    const aHotels = this.props.state.hotel.allHotels ?
      this.props.state.hotel.allHotels : [];
    if (this.state.amenities === "all") {
      this.props.setHotelsFilter(aHotels);
      return;
    }

    let fHotels = aHotels.filter(fh => {
      for(let x = 0; x < fh.amenities.length; x++) {
        if (fh.amenities[x] === this.state.amenities) {
          return fh
        }
      }
      return null;
    });
    this.props.setHotelsFilter(fHotels);
  }

  viewItem = async(item) => {
    await this.props.setHotelCurrent(item);
    this.props.showDetails();
  }

  getUser = () => {
    this.props.setAuthInfo({ first_name: 'John', last_name: 'Doe' });
  }

  getHotels = async() => {
    const requestResp = await sendRequest('get', 'hotels', null);
    this.props.setHotelsList(requestResp.data);
    this.props.setHotelsFilter(requestResp.data);
  }

  toggleMore = (tog) => {
    if (tog === 'show') {
      this.setState({ more: 'flex', width: '45%' })
    } else {
      this.setState({ more: 'none', width: '65%' })
    }
  }

  render() {
    const hotelList = this.props.state.hotel.filteredHotels ?
      this.props.state.hotel.filteredHotels : [];
    const lang = en.page.home;
    return (
      <div className="IndexRow">
      <div style={{ marginBottom: '15px' }}>
        <b>{hotelList.length + ' Hotels found'}</b>
      </div>
        <div>
          <span className="Btn">
            <select
              className=""
              name="filter-distance"
              value={this.state.distance}
              onChange={this.filterDistance}>
                <option value="1001">{lang.hotelFilter.distance}</option>
                <option value="100">100m</option>
                <option value="200">200m</option>
                <option value="500">500m</option>
                <option value="1000">1000m</option>
            </select>
          </span>
          <span className="Btn">
            <select
              className=""
              name="filter-rating"
              value={this.state.rating}
              onChange={this.filterRating}>
                <option value="5">{lang.hotelFilter.rating}</option>
                <option value="5">{'5 ' + lang.hotelFilter.ratingUnit}</option>
                <option value="4">{'4 ' + lang.hotelFilter.ratingUnit}</option>
                <option value="3">{'3 ' + lang.hotelFilter.ratingUnit}</option>
                <option value="2">{'2 ' + lang.hotelFilter.ratingUnit}</option>
                <option value="1">{'1 ' + lang.hotelFilter.ratingUnit}</option>
            </select>
          </span>
          <span className="Btn">
            <select
              className=""
              name="filter-price"
              value={this.state.price_category}
              onChange={this.filterPrice}>
                <option value="all">{lang.hotelFilter.price}</option>
                <option value="high">{lang.hotelFilter.priceType.priceH}</option>
                <option value="medium">
                  {lang.hotelFilter.priceType.priceM}
                </option>
                <option value="low">{lang.hotelFilter.priceType.priceL}</option>
            </select>
          </span>
          <span className="Btn">
          <select
            className=""
            name="filter-amenities"
            value={this.state.amenities}
            onChange={this.filterAmenities}>
              <option value="all">{lang.hotelFilter.facilities}</option>
              <option value="free parking">
              {lang.hotelFilter.facilitiesList.park}</option>
              <option value="free wifi">
                {lang.hotelFilter.facilitiesList.wifi}
              </option>
              <option value="pets">
                {lang.hotelFilter.facilitiesList.pets}
              </option>
              <option value="restaurant">
                {lang.hotelFilter.facilitiesList.rest}
              </option>
              <option value="gym">
                {lang.hotelFilter.facilitiesList.gym}
              </option>
              <option value="pool">
                {lang.hotelFilter.facilitiesList.pool}
              </option>
              <option value="spa">{lang.hotelFilter.facilitiesList.spa}</option>
          </select>
          </span>
          <span className="Btn">
            <button onClick={this.getHotels}>{lang.ref}</button>
          </span>
        </div>
        <div>
          {
            hotelList.map(item => {
              let rate = Math.round(item.rating);
              let rateArray = [];
              for (let r = 0; r < rate; r ++) {
                rateArray.push(r);
              }
              const noStar = () => {
                return rate < 1 ?
                <span>
                  <img
                    alt="no-star"
                    src={require("../images/no-star-icon.png")}
                    style={{width:'15%'}} />
                </span> : null
              }

              return (
                <div
                  key={item.id}
                  className="IndexList"
                  onMouseOver={() => this.toggleMore('show')}
                  onMouseOut={() => this.toggleMore('hide')}
                  onClick={() => this.viewItem(item)}>
                    <div className="ListImage">
                      <img src={require("../images/" + item.images[0])}
                        className="MainImage"
                        alt={item.images[0]} />
                    </div>
                    <div
                      className="ListInfo"
                      style={{ width: this.state.width }}>
                        <h2>{item.name}</h2>
                        <h3>{item.description}</h3>
                        <h4>
                          {
                            en.page.home.hotelDistance
                            + ': '
                            + item.distance_to_venue
                            + 'm'
                          }
                        </h4>
                    </div>
                    <div
                      className="MoreInfo"
                      style={{ display: this.state.more }}>
                        <h3>{en.page.home.rate}</h3>
                        <div>
                          {
                            rateArray.map(rate => {
                              return <span key={rate}>
                                <img
                                  alt="star"
                                  className="MoreImage"
                                  src={require("../images/star-icon.png")}
                                  style={{width:'15%'}} />
                              </span>
                            })
                          }
                          {noStar()}
                        </div>
                    </div>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}
const IndexRow = connect(mapStateToProps, mapDispatchToProps)(IndexRowComp);
export default IndexRow;
