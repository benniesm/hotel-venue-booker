import React from 'react';
import { Provider } from 'react-redux';
import { persistor, store } from './store/Store';
import { PersistGate } from 'redux-persist/lib/integration/react';
import './App.css';
import PageContainer from './layout/PageContainer';

function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <PageContainer />
      </PersistGate>
    </Provider>
  );
}

export default App;
