const en = {
  footer: {
    text: 'Developers\' Conference'
  },
  menu: {
    home: 'Home',
    view: 'Hotel Details'
  },
  page: {
    home: {
      title: 'Recommended Hotels',
      hotelDistance: 'DISTANCE',
      hotelFilter: {
        distance: 'FILTER BY DISTANCE (MAX)',
        rating: 'FILTER BY RATING (MAX)',
        ratingUnit: 'Star',
        price: 'FILTER BY PRICE',
        priceType: {
          priceH: 'High',
          priceM: 'Medium',
          priceL: 'Low'
        },
        facilities: 'FILTER BY FACILITIES',
        facilitiesList: {
          park: 'Free Parking',
          wifi: 'Free WIFI',
          pets: 'Pets',
          rest: 'Restaurant',
          gym: 'Gym',
          pool: 'Pool',
          spa: 'Spa'
        }
      },
      ref: 'REFRESH LIST',
      rate: 'RATING'
    },
    view: {
      info: 'Hotel Information',
      dist: 'Distance to Venue',
      price: 'Price Level',
      fac: 'Amenities',
      infoRm: 'Room description',
      occup: 'Maximum Room Occupancy',
      priceRm: 'Price',
      name: 'Name',
      rm: 'Available Rooms',
      bk: 'Book Now',
      more: 'Show More Rooms'
    },
    conf: {
      title: 'Booking Confirmation'
    }
  },
  user: {
    message: 'Welcome'
  }
}

export default en;
