const defaultState = {
  info: {},
  auth: {},
  num: null
}

const pageReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'PAGE_DATA':
      return {
        info: action.info,
        auth: state.auth,
        num: state.num
      };
    case 'AUTH_DATA':
      return {
        auth: action.auth,
        info: state.info,
        num: state.num
      };
    case 'BOOKING':
      return {
        num: action.num,
        info: state.info,
        auth: state.auth
      };
    default:
      return state;
  }
}

export default pageReducer;
