const defaultState = {
  allHotels: [],
  filteredHotels: [],
  currentHotel: {}
}

const hotelReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'HOTELS_LIST':
      return {
        allHotels: action.allHotels,
        currentHotel: state.currentHotel,
        filteredHotels: state.filteredHotels
      };
    case 'HOTELS_FILTERED':
      return {
        filteredHotels: action.filteredHotels,
        allHotels: state.allHotels,
        currentHotel: state.currentHotel
      };
    case 'HOTEL_ID':
      return {
        currentHotel: action.currentHotel,
        allHotels: state.allHotels,
        filteredHotels: state.filteredHotels
      };
    default:
      return state;
  }
}

export default hotelReducer;
