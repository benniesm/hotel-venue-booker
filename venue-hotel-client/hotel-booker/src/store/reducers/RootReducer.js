import { combineReducers } from 'redux';
import pageReducer from './PageReducer';
import hotelReducer from './HotelReducer';

const rootReducer = combineReducers({
  page: pageReducer,
  hotel: hotelReducer
});

export default rootReducer;
