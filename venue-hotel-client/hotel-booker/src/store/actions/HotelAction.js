const hotelsData = (data) => {
  return {
    type: 'HOTELS_LIST',
    allHotels: data
  }
}

const hotelsFilter = (data) => {
  return {
    type: 'HOTELS_FILTERED',
    filteredHotels: data
  }
}

const hotelId = (data) => {
  return {
    type: 'HOTEL_ID',
    currentHotel: data
  }
}

export { hotelsData, hotelsFilter, hotelId };
