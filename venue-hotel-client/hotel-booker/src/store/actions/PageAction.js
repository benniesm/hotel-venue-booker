const authData = (data) => {
  return {
    type: 'AUTH_DATA',
    auth: data
  }
}

const booking = (data) => {
  return {
    type: 'BOOKING',
    num: data
  }
}

const pageData = (data) => {
  return {
    type: 'PAGE_DATA',
    info: data
  }
}

export { authData, booking, pageData };
