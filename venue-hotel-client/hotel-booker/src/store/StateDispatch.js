import { authData, booking, pageData } from './actions/PageAction';
import { hotelsData, hotelsFilter, hotelId } from './actions/HotelAction';

const mapStateToProps = (state) => {
  return { state: state }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setAuthInfo: (data) => {
      dispatch(authData(data))
    },
    setBookingNum: (data) => {
      dispatch(booking(data))
    },
    setPageInfo: (data) => {
      dispatch(pageData(data))
    },
    setHotelsList: (data) => {
      dispatch(hotelsData(data))
    },
    setHotelsFilter: (data) => {
      dispatch(hotelsFilter(data))
    },
    setHotelCurrent: (data) => {
      dispatch(hotelId(data))
    }
  }
}

export { mapStateToProps, mapDispatchToProps };
