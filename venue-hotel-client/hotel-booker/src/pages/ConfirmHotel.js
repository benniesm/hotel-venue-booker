import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  mapStateToProps,
  mapDispatchToProps
} from '../store/StateDispatch';
import en from '../languageData/English';

class ConfirmHotelComp extends Component {
  render() {
    const page = this.props.state.page;
    const authInfo = page.auth;
    const num = page.num;
    const lang = en.page;

    return (
      <div className="IndexPage">
        <div className="ViewBody">
          <h1>{lang.conf.title}</h1>
          <table className="TabView" cellSpacing="0">
            <tbody>
              <tr>
                <td className="TabRowHead">Booking ID</td>
                <td className="TabRowInfo">
                  {num}
                </td>
              </tr>
              <tr>
                <td className="TabRowHead">{lang.view.name}</td>
                <td className="TabRowInfo">
                  {authInfo.first_name + ' ' + authInfo.last_name}
                </td>
              </tr>
              <tr>
                <td className="TabRowHead">{lang.view.info}</td>
                <td className="TabRowInfo">
                  {this.props.state.hotel.currentHotel.name}
                </td>
              </tr>
              <tr>
                <td className="TabRowHead">{lang.view.infoRm}</td>
                <td className="TabRowInfo">
                  {page.info.name}
                </td>
              </tr>
              <tr>
                <td className="TabRowHead">{lang.view.occup}</td>
                <td className="TabRowInfo">
                  {page.info.max_occupancy}
                </td>
              </tr>
              <tr>
                <td className="TabRowHead">{lang.view.priceRm}</td>
                <td className="TabRowInfo">
                  {'$ ' + page.info.price_in_usd.toFixed(2)}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

const ConfirmHotel = connect(mapStateToProps, mapDispatchToProps)(ConfirmHotelComp);
export default ConfirmHotel;
