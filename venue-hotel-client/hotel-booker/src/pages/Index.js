import React, { Component } from 'react';
import en from '../languageData/English';
import IndexRow from '../components/IndexRow';

class Index extends Component {
  showDetails = () => {
    this.props.history.push('/view-hotel');
  }

  render() {
    return (
      <div className="IndexPage">
      <h1>{en.page.home.title}</h1>
      <IndexRow showDetails={this.showDetails} />
      </div>
    );
  }
}

export default Index;
