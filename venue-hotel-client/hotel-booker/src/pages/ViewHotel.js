import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  mapStateToProps,
  mapDispatchToProps
} from '../store/StateDispatch';
import en from '../languageData/English';

class ViewHotelComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rooms: this.props.state.hotel.currentHotel.rooms.slice(0,2),
      display: 'inline'
    }
  }

  bookNow = (rm) => {
    //Generate random booking ID
    const str = ['A','B','C','D','E','F'];
    let strings = '';
    for (let c = 0; c < 5; c++) {
      strings += str[Math.floor(Math.random() * 6)];
    }

    const num =  strings + Math.floor(Math.random() * 999999) + 1000000;

    this.props.setPageInfo(rm);
    this.props.setBookingNum(num);
    this.props.history.push('/confirm-hotel');
  }

  showMore = () => {
    this.setState({ rooms: this.props.state.hotel.currentHotel.rooms,
      display: 'none'
    })
  }

  render() {
    const currHotel = this.props.state.hotel.currentHotel;
    const lang = en.page;

    return (
      <div className="IndexPage">
        <div className="ViewBody">
          <div className="PageTitle">
            <h2>{currHotel.name}</h2>
          </div>
          <div className="ViewImage">
            <div className="ViewImageSet">
              <img src={require("../images/" + currHotel.images[0])}
                alt={currHotel.images[0]}
                className="vImg" />
            </div>
            <div className="ViewImageSet">
              <img src={require("../images/" + currHotel.images[1])}
              alt={currHotel.images[1]}
                className="vImg" />
            </div>
            <div className="ViewImageSet">
              <img src={require("../images/" + currHotel.images[2])}
                alt={currHotel.images[2]}
                className="vImg" />
            </div>
          </div>
          <div className="ViewInfo">
            <table
              className="TabView"
              cellSpacing="0"
              style={{
                marginBottom: 0,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0
              }}>
              <tbody>
                <tr>
                  <td className="TabRowHead">{lang.view.info}</td>
                  <td className="TabRowInfo">{currHotel.description}</td>
                </tr>
                <tr>
                  <td className="TabRowHead">{lang.view.dist}</td>
                  <td className="TabRowInfo">
                    {currHotel.distance_to_venue + 'm'}
                  </td>
                </tr>
                <tr>
                  <td className="TabRowHead">
                    {lang.home.hotelFilter.ratingUnit}
                  </td>
                  <td className="TabRowInfo">{currHotel.rating.toFixed(1)}</td>
                </tr>
                <tr>
                  <td className="TabRowHead">{lang.view.price}</td>
                  <td className="TabRowInfo">{currHotel.price_category}</td>
                </tr>
                <tr>
                  <td className="TabRowHead TabRowHeadLast">{lang.view.fac}</td>
                  <td className="TabRowInfo TabRowInfoLast">
                  {
                    currHotel.amenities.map(a => {
                      return (
                        <span key={a}>{a}<br/></span>
                      )
                    })
                  }
                  </td>
                </tr>
              </tbody>
            </table>
            <table
              className="TabView"
              cellSpacing="0"
              style={{
                marginTop: 0,
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0
              }}>
              <thead>
                <tr>
                  <td colSpan="5" style={{ textAlign: 'center' }}>
                    <h2 style={{ marginLeft: '10px', marginTop: '30px' }}>
                      {lang.view.rm}
                    </h2>
                  </td>
                </tr>
                <tr>
                <th className="TabViewHead">{lang.view.name}</th>
                <th className="TabViewHead">{lang.view.infoRm}</th>
                <th className="TabViewHead">{lang.view.occup}</th>
                <th className="TabViewHead">{lang.view.priceRm}</th>
                <th className="TabViewHead"></th>
                </tr>
              </thead>
              <tbody>
                {
                  this.state.rooms.sort((a,b) =>
                    a.price_in_usd > b.price_in_usd
                  ).map(rm => {
                    return (
                      <tr key={rm.id} className="TabViewRow">
                        <td className="TabViewCol">{rm.name}</td>
                        <td className="TabViewCol">{rm.description}</td>
                        <td className="TabViewCol">{rm.max_occupancy}</td>
                        <td className="TabViewCol">
                          {'$ ' + rm.price_in_usd.toFixed(2)}
                        </td>
                        <td className="TabViewCol">
                          <button onClick={() => this.bookNow(rm)}>
                            {lang.view.bk}
                          </button>
                        </td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
            <div style={{ display: this.state.display }}>
              <button
                style={{
                  width: '200px',
                  height: '50px',
                  margin: 2,
                  color: 'white',
                  borderRadius: '6px',
                  borderWidth: 1,
                  backgroundColor: 'indigo'
                }}
                onClick={this.showMore}>
                {lang.view.more}
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const ViewHotel = connect(mapStateToProps, mapDispatchToProps)(ViewHotelComp);
export default ViewHotel;
