import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  mapStateToProps,
  mapDispatchToProps
} from '../store/StateDispatch';
import sendRequest from '../api/SendRequest';
import en from '../languageData/English';

class AdminIndexComp extends Component {
  createItem = () => {
      this.props.history.push('/create-hotel');
  }

  removeItem = async(id) => {
    await sendRequest('delete', 'hotels/' + id, null);
    const requestResp = await sendRequest('get', 'hotels', null);
    this.props.setHotelsList(requestResp.data);
    this.props.setHotelsFilter(requestResp.data);
  }

  render() {
    const hotels = this.props.state.hotel.allHotels;
    const lang = en.page;
    return (
      <div className="IndexPage">
        <div className="ViewBody">
          <div><h2>Booking Admin</h2></div>
          <div><button onClick={this.createItem}>Create Hotel</button></div>
          <div style={{ width: '80%' }}>
            <table className="TabView TabAdmin" cellSpacing="0">
              <thead>
                <tr>
                <th className="TabViewHead">{lang.view.name}</th>
                <th className="TabViewHead"></th>
                </tr>
              </thead>
              <tbody>
                {
                  hotels.map(hotel => {
                    return (
                      <tr key={hotel.id} className="TabViewRow">
                        <td className="TabViewCol">{hotel.name}</td>
                        <td className="TabViewCol">
                          <button
                            onClick={() => {
                              if (window.confirm(
                                'Remove this hotel from list?'
                              ))
                              this.removeItem(hotel.id) }}>X</button>
                        </td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

const AdminIndex = connect(mapStateToProps, mapDispatchToProps)(AdminIndexComp);
export default AdminIndex;
