import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  mapStateToProps,
  mapDispatchToProps
} from '../store/StateDispatch';
import uuid4 from 'uuid4';
import history from '../pages/History';
import sendRequest from '../api/SendRequest';

class CreateHotelComp extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      description: '',
      distance: '',
      rating: '',
      price_category: '',
      amenities: ''
    }
    this.changeValue = this.changeValue.bind(this);
  }

  changeValue = (evt) => {
    this.setState({
      [evt.target.name]: evt.target.value
    })
  }

  createNew = async() => {
    let requestResp = await sendRequest('post', 'hotels', {
      id: uuid4(),
      name: this.state.name,
      description: this.state.description,
      distance: parseInt(this.state.distance),
      rating: this.state.rating,
      price_category: parseFloat(this.state.price_category),
      amenities: this.state.amenities.split(",")
    });

    if(requestResp.status === 201) {
      const requestResp2 = await sendRequest('get', 'hotels', null);
      this.props.setHotelsList(requestResp2.data);
      this.props.setHotelsFilter(requestResp2.data);
      history.push('/admin-index');
    }
  }

  render() {
    return (
      <div className="IndexPage">
        <div className="ViewBody">
          <h2>Create New Hotel</h2>
          <input
            className="InputText"
            type="text"
            name="name"
            placeholder="Hotel Name"
            value={this.state.name}
            onChange={this.changeValue}
          />
          <input
            className="InputText"
            type="text"
            name="description"
            placeholder="Hotel Information"
            value={this.state.description}
            onChange={this.changeValue}
          />
          <input
            className="InputText"
            type="text"
            name="distance"
            placeholder="Distance to Venue"
            value={this.state.distance}
            onChange={this.changeValue}
          />
          <input
            className="InputText"
            type="text"
            name="rating"
            placeholder="Rating"
            value={this.state.rating}
            onChange={this.changeValue}
          />
          <select
            className="InputText"
            name="price_category"
            value={this.state.price_category}
            onChange={this.changeValue}>
              <option value="">Please Select</option>
              <option value="high">High</option>
              <option value="medium">Medium</option>
              <option value="low">Low</option>
          </select>
          <textarea
            className="InputText"
            type="text"
            name="amenities"
            placeholder="Separate multiple values by comma"
            value={this.state.amenities}
            onChange={this.changeValue}>
          </textarea>
          <button onClick={this.createNew}>Submit</button>
        </div>
      </div>
    )
  }
}

const CreateHotel = connect(mapStateToProps, mapDispatchToProps)(CreateHotelComp);
export default CreateHotel;
